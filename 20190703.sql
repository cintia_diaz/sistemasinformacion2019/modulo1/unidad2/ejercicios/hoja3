﻿USE ciclistas;
/*
  H3 - Consultas de seleccion 3
*/

-- 1.1 Listar las edades de todos los ciclistas de Banesto
  SELECT 
    DISTINCT c.edad 
  FROM ciclista c WHERE c.nomequipo='Banesto';

-- 1.2 Listar las edades de los ciclistas que son de Banesto o de Navigare
  SELECT
    DISTINCT c.edad 
  FROM ciclista c 
  WHERE c.nomequipo='Banesto' 
  OR c.nomequipo='Navigare';

-- 1.3 Listar el dorsal de los ciclistas que son de Banesto y cuya edad está entre 25 y 32
  SELECT 
    c.dorsal 
  FROM ciclista c 
  WHERE c.nomequipo = 'Banesto' 
  AND c.edad BETWEEN 25 AND 32;

SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';
SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 25 AND 32;

SELECT * FROM (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
  )c1
  JOIN (
    SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 25 AND 32
  ) c2
  USING (dorsal)
;


-- 1.4 Listar el dorsal de los ciclistas que son de Banesto o cuya edad está entre 25 y 32
  SELECT 
    c.dorsal 
  FROM ciclista c 
  WHERE c.nomequipo='Banesto' 
  OR c.edad BETWEEN 25 AND 32;

-- 1.5 Listar la inicial del equipo de los ciclistas cuyo nombre comience por R
  SELECT
    DISTINCT LEFT(c.nomequipo, 1) 
  FROM ciclista c 
  WHERE c.nombre LIKE 'R%';


-- utilizando la funcion LEFT
  SELECT 
    DISTINCT LEFT(c.nomequipo,1) 
  FROM ciclista c 
  WHERE LEFT(c.nombre, 1)='R';

-- 1.6 Listar el código de las etapas que su salida y llegada sea en la misma población
  SELECT 
    e.numetapa 
  FROM etapa e 
  WHERE e.salida=e.llegada;

-- 1.7 Listar el código de las etapas que su salida y llegada no sean en la misma población
-- y que conozcamos el dorsal del ciclista que ha ganado la etapa
  SELECT 
    e.numetapa 
  FROM etapa e 
  WHERE e.salida<>llegada 
  AND e.dorsal IS NOT NULL;


-- 1.8 Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400
  SELECT 
    p.nompuerto 
  FROM puerto p 
  WHERE p.altura BETWEEN 1000 AND 2000 
  OR p.altura > 2400;


-- 1.9 Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000
  --   o que la altura sea mayor que 2400
  SELECT DISTINCT p.dorsal
  FROM puerto p 
  WHERE p.altura BETWEEN 1000 AND 2000 
  OR p.altura >2400;


-- 1.10 Listar el número de ciclistas que hayan ganado alguna etapa
  SELECT 
    COUNT(DISTINCT e.dorsal) 
  FROM etapa e;
  

-- 1.11 Listar el número de etapas que tengan puerto
  SELECT 
    COUNT(DISTINCT p.numetapa) 
  FROM puerto p;


-- 1.12 Listar el número de ciclistas que hayan ganado algún puerto
  SELECT 
    COUNT(DISTINCT p.dorsal) 
  FROM puerto p;

-- 1.13 Listar el código de la etapa con el número de puertos que tiene
  SELECT 
    p.numetapa, COUNT(p.nompuerto) numeroPuertos 
  FROM puerto p 
  GROUP BY p.numetapa;

-- 1.14 Indicar la altura media de los puertos
  SELECT 
    AVG(p.altura) 
  FROM puerto p;

-- 1.15 Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500
  SELECT
    p.numetapa, AVG(p.altura) m 
  FROM puerto p 
  GROUP BY p.numetapa 
  HAVING m>1500;

  SELECT 
    p.numetapa 
  FROM puerto p 
  GROUP BY p.numetapa 
  HAVING AVG(p.altura)>1500;


-- 1.16 Indicar el número de etapas que cumplen la condición anterior
  SELECT 
    COUNT(*) 
    FROM (
          SELECT p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG(p.altura)>1500
         ) c1;


-- 1.17 Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot
  SELECT 
    dorsal, COUNT(*) numeroVeces 
  FROM lleva l 
  GROUP BY l.dorsal;

-- 1.18 Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot
  SELECT 
    l.dorsal, l.código, COUNT(*) veces 
  FROM lleva l 
  GROUP BY l.dorsal, l.código;

-- 1.19 Listar el dorsal, el código de etapa, el ciclista y el número de maillots que ese ciclista ha llevado en cada etapa.
  SELECT 
    dorsal, l.numetapa, COUNT(*) numero 
  FROM lleva l 
  GROUP BY l.dorsal, l.numetapa;

  SELECT c1.dorsal, c1.numetapa, c.nombre, c1.numero 
    FROM 
    (
    SELECT dorsal, l.numetapa, COUNT(*) numero FROM lleva l GROUP BY l.dorsal, l.numetapa
    ) c1
    JOIN ciclista c
   USING (dorsal);